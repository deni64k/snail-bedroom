#!/bin/sh

. ./ffmpeg.conf

${CAPTURE} -c -1 -o | ${FFMPEG} -v 100 -an -i - \
  -vcodec copy -f h264 "tcp://${vstream_to}?buffer_size=65535"
