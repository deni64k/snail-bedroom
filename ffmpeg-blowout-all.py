#!/usr/bin/env python3

import pprint
import os
import shlex, subprocess

ADDR_SUCKER  = os.environ['ADDR_SUCKER']
ADDR_BEDROOM = os.environ['ADDR_BEDROOM']
RTMPSERVER   = os.environ['RTMPSERVER']

FFMPEG      = os.environ['FFMPEG']
HLS_PATH    = os.environ['HLS_PATH']

STREAM_NAME = os.environ['STREAM_NAME']
SIZES       = os.environ['SIZES'].split(' ')
FPS         = os.environ['FPS']

ffmpeg_cmd  = [FFMPEG]
ffmpeg_cmd += shlex.split('-c:a aac -i "http://mp4.somafm.com:8100"')
ffmpeg_cmd += shlex.split('-c:v h264 -f h264 -i "tcp://%s:40000?fifo_size=50000000&overrun_nonfatal=1"' % (ADDR_SUCKER))

video_params = shlex.split('-pix_fmt yuv420p -flags -global_header')
x264_params  = shlex.split('-c:v libx264 -q:v 4 -threads 8 -preset veryfast -tune zerolatency -bsf:v h264_mp4toannexb') + video_params
flv_params   = shlex.split('-c:v flv1 -q:v 1') + video_params

for size in SIZES:
  ffmpeg_cmd += shlex.split('-map 0 -c:a libmp3lame -q:a 1 -map 1 -s %s' % (size)) + flv_params
  ffmpeg_cmd += shlex.split('-f flv -metadata streamName="%s-%s" "tcp://%s/live?pkt_size=1316"' % (STREAM_NAME, size, RTMPSERVER))

  ffmpeg_cmd += shlex.split('-map 0 -c:a aac -q:a 1 -bsf:a aac_adtstoasc -strict experimental -map 1 -s %s' % (size)) + x264_params
  ffmpeg_cmd += shlex.split("""
    -f segment -segment_list "%(stream_name)s-%(size)s.m3u8"
               -segment_list_type m3u8
               -segment_list_size 3
               -segment_wrap 24
               -segment_time 30
               -segment_format mpegts
               -segment_list_flags +live
               "%(stream_name)s-%(size)s-segment-%%03d.ts"
  """ % {'stream_name': STREAM_NAME, 'size': size})

# ffmpeg_cmd += shlex.split('-map 0 -map 1 -threads 16 http://localhost:60789/snail.ffm')

os.chdir(HLS_PATH)
print('Running ffmpeg:')
print(' '.join(ffmpeg_cmd))

subprocess.check_call(ffmpeg_cmd)
