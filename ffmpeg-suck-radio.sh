#!/bin/sh

. ./ffmpeg.conf

${FFMPEG} -i "http://mp4.somafm.com:8100" \
          -map 0 -c:a aac -bsf:a aac_adtstoasc -strict experimental -flags -global_header -f mpegts udp://localhost:40001 \
          -map 0 -c:a libmp3lame -flags -global_header -f mpegts udp://localhost:40002
