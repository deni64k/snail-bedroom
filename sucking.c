#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <sys/types.h>
#include <sys/stat.h>

#include <sys/stat.h>
#include <sys/socket.h>
#include <signal.h>
#include <fcntl.h>
#include <unistd.h>
#include <dirent.h>

#include <event2/event.h>
#include <event2/http.h>
#include <event2/buffer.h>
#include <event2/util.h>
#include <event2/keyvalq_struct.h>

char uri_root[512];

/* Callback used for the /dump URI, and for every non-GET request:
 * dumps all information to stdout and gives back a trivial 200 ok */
static void
dump_request_cb(struct evhttp_request *req, void *arg)
{
  const char *cmdtype;
  struct evkeyvalq *headers;
  struct evkeyval *header;
  struct evbuffer *buf;

  switch (evhttp_request_get_command(req)) {
    case EVHTTP_REQ_GET: cmdtype = "GET"; break;
    case EVHTTP_REQ_POST: cmdtype = "POST"; break;
    case EVHTTP_REQ_HEAD: cmdtype = "HEAD"; break;
    case EVHTTP_REQ_PUT: cmdtype = "PUT"; break;
    case EVHTTP_REQ_DELETE: cmdtype = "DELETE"; break;
    case EVHTTP_REQ_OPTIONS: cmdtype = "OPTIONS"; break;
    case EVHTTP_REQ_TRACE: cmdtype = "TRACE"; break;
    case EVHTTP_REQ_CONNECT: cmdtype = "CONNECT"; break;
    case EVHTTP_REQ_PATCH: cmdtype = "PATCH"; break;
    default: cmdtype = "unknown"; break;
  }

  printf("Received a %s request for %s\nHeaders:\n",
      cmdtype, evhttp_request_get_uri(req));

  headers = evhttp_request_get_input_headers(req);
  for (header = headers->tqh_first; header;
      header = header->next.tqe_next) {
    printf("  %s: %s\n", header->key, header->value);
  }

  buf = evhttp_request_get_input_buffer(req);
  puts("Input data: <<<");
  while (evbuffer_get_length(buf)) {
    int n;
    char cbuf[128];
    n = evbuffer_remove(buf, cbuf, sizeof(buf)-1);
    if (n > 0)
      (void) fwrite(cbuf, 1, n, stdout);
  }
  puts(">>>");

  evhttp_send_reply(req, 200, "OK", NULL);
}

static void
usage()
{
  fprintf(stderr, "Syntax: sucking <docroot>\n");
}

int
main(int argc, char **argv) {
  struct event_base *base;
  struct evhttp *http;
  struct evhttp_bound_socket *handle;

  unsigned short port = 0;

  if (signal(SIGPIPE, SIG_IGN) == SIG_ERR)
    return 1;

  if (argc < 2) {
    usage();
    return 1;
  }

  base = event_base_new();
  if (!base) {
    fprintf(stderr, "Couldn't create an event_base: exiting\n");
    return 1;
  }

  /* Create a new evhttp object to handle requests. */
  http = evhttp_new(base);
  if (!http) {
    fprintf(stderr, "couldn't create evhttp. Exiting.\n");
    return 1;
  }

  /* The /dump URI will dump all requests to stdout and say 200 ok. */
  evhttp_set_cb(http, "/dump", dump_request_cb, NULL);

  /* Now we tell the evhttp what port to listen on */
  handle = evhttp_bind_socket_with_handle(http, "0.0.0.0", port);
  if (!handle) {
    fprintf(stderr, "couldn't bind to port %d. Exiting.\n",
        (int)port);
    return 1;
  }

  {
    /* Extract and display the address we're listening on. */
    struct sockaddr_storage ss;
    evutil_socket_t fd;
    ev_socklen_t socklen = sizeof(ss);
    char addrbuf[128];
    void *inaddr;
    const char *addr;
    int got_port = -1;
    fd = evhttp_bound_socket_get_fd(handle);
    memset(&ss, 0, sizeof(ss));
    if (getsockname(fd, (struct sockaddr *)&ss, &socklen)) {
      perror("getsockname() failed");
      return 1;
    }
    if (ss.ss_family == AF_INET) {
      got_port = ntohs(((struct sockaddr_in*)&ss)->sin_port);
      inaddr = &((struct sockaddr_in*)&ss)->sin_addr;
    } else if (ss.ss_family == AF_INET6) {
      got_port = ntohs(((struct sockaddr_in6*)&ss)->sin6_port);
      inaddr = &((struct sockaddr_in6*)&ss)->sin6_addr;
    } else {
      fprintf(stderr, "Weird address family %d\n",
          ss.ss_family);
      return 1;
    }
    addr = evutil_inet_ntop(ss.ss_family, inaddr, addrbuf,
        sizeof(addrbuf));
    if (addr) {
      printf("Listening on %s:%d\n", addr, got_port);
      evutil_snprintf(uri_root, sizeof(uri_root),
          "http://%s:%d",addr,got_port);
    } else {
      fprintf(stderr, "evutil_inet_ntop failed\n");
      return 1;
    }
  }

  event_base_dispatch(base);

  return 0;
}