#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include "libavcodec/avcodec.h"
#include "libavutil/mathematics.h"

int try_decode_video() {
  char const *filename = "http://localhost:8100";

  AVCodec *codec;
  AVCodecContext *c = NULL;
  AVFrame *picture;
  FILE *f;

  codec = avcodec_find_decoder(AV_CODEC_ID_H264);

  if (!codec) {
    fprintf(stderr, "codec not found\n");
    exit(1);
  }

  c = avcodec_alloc_context();
  picture = avcodec_alloc_frame();

  if (codec->capabilities & CODEC_CAP_TRUNCATED)
    c->flags|= CODEC_FLAG_TRUNCATED; /* we do not send complete frames */

   /* For some codecs, such as msmpeg4 and mpeg4, width and height
      MUST be initialized there because this information is not
      available in the bitstream. */

  /* open it */
  if (avcodec_open(c, codec) < 0) {
    fprintf(stderr, "could not open codec\n");
    exit(1);
  }

  /* the codec gives us the frame size, in samples */

  f = fopen(filename, "rb");
  if (!f) {
    fprintf(stderr, "could not open %s\n", filename);
    exit(1);
  }
}

int main(int argc, char **argv) {
  avcodec_init();
  avcodec_register_all();
  avformat_network_init();

  avformat_network_deinit();

  return 0;
}
